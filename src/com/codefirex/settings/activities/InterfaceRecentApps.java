package com.codefirex.settings.activities;

import android.content.ContentResolver;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.util.Log;

import com.codefirex.settings.R;
import com.codefirex.settings.SettingsFragment;

public class InterfaceRecentApps extends SettingsFragment {

    private static final String PREF_RECENTS_CLEAR_ALL = "recents_clear_all";
    private static final String PREF_RECENTS_MEM_DISPLAY = "recents_mem_display";

    private ContentResolver mCr;
    private PreferenceScreen mPrefSet;

    private CheckBoxPreference mClearAll;
    private CheckBoxPreference mMemDisplay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.interface_recent_apps);

        mPrefSet = getPreferenceScreen();
        mCr = getContentResolver();

        /* Clear All */
        mClearAll = (CheckBoxPreference) mPrefSet.findPreference(
                PREF_RECENTS_CLEAR_ALL);
        mClearAll.setChecked(Settings.System.getInt(mCr,
                    Settings.System.RECENTS_CLEAR_ALL, 0) == 1);

        /* Memory Display */
        mMemDisplay = (CheckBoxPreference) mPrefSet.findPreference(
                PREF_RECENTS_MEM_DISPLAY);
        mMemDisplay.setChecked(Settings.System.getInt(mCr,
                    Settings.System.RECENTS_MEM_DISPLAY, 0) == 1);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, 
    		Preference preference) {
        
        if (preference == mClearAll) {
            Settings.System.putInt(mCr, Settings.System.RECENTS_CLEAR_ALL,
                    ((CheckBoxPreference) preference).isChecked() ? 1 : 0);
            return true;
        } else if (preference == mMemDisplay) {
            Settings.System.putInt(mCr, Settings.System.RECENTS_MEM_DISPLAY,
                    ((CheckBoxPreference) preference).isChecked() ? 1 : 0);
            return true;
        }

        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
}

