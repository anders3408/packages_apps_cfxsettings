package com.codefirex.settings.activities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ActivityManager;
import android.content.Context;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.preference.Preference.OnPreferenceChangeListener;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Spannable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.codefirex.settings.R;
import com.codefirex.settings.SettingsFragment;

public class InterfaceStatusBar extends SettingsFragment
    implements OnPreferenceChangeListener {

    private final static String TAG = InterfaceButtons.class.getSimpleName();

    private static final String STATUSBAR_BATTERY_STYLE = "pref_statusbar_batt_style";
    private static final String PREF_STATUS_BAR_NOTIF_COUNT = "status_bar_notif_count";
//    private static final String NOTIFICATION_SHADE_DIM = "notification_shade_dim";

    private static final int BATT_STOCK = 0;
    private static final int BATT_PERCENT = 1;
    private static final int BATT_HIDDEN = 2;

    private ContentResolver mCr;
    private Context mContext;
//    private CheckBoxPreference mNotificationShadeDim;
    private PreferenceScreen mPrefSet;
    private ListPreference mNotificationsBeh;
    private ListPreference mBattStyle;

    CheckBoxPreference mStatusBarNotifCount;
    CheckBoxPreference mOperatorIcon;
    ListPreference mStatusBarBeh;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.interface_statusbar);

        mContext = getActivity();
        mPrefSet = getPreferenceScreen();
        mCr = getContentResolver();

        /* Battery Icon Styles */
        final int battCurStyle = Settings.System.getInt(mCr, Settings.System.STATUSBAR_BATT_STYLE, BATT_PERCENT);
        mBattStyle = (ListPreference) mPrefSet.findPreference(STATUSBAR_BATTERY_STYLE);
        mBattStyle.setValue(String.valueOf(battCurStyle));
        mBattStyle.setOnPreferenceChangeListener(this);

        /* Show Notification Count */
        mStatusBarNotifCount = (CheckBoxPreference) mPrefSet.findPreference(
                PREF_STATUS_BAR_NOTIF_COUNT);
        mStatusBarNotifCount.setChecked(Settings.System.getInt(mCr,
                Settings.System.STATUSBAR_NOTIF_COUNT,0)==1);
        mStatusBarNotifCount.setOnPreferenceChangeListener(this);

        mOperatorIcon = (CheckBoxPreference) mPrefSet.findPreference(
                "status_bar_use_opicon");
        mOperatorIcon.setChecked(Settings.Secure.getInt(mCr,
                Settings.Secure.USE_OPERATOR_ICON,0)==1);
        mOperatorIcon.setOnPreferenceChangeListener(this);
        
        /* Notification Expand Behavior */
        int CurrentBeh = Settings.Secure.getInt(mCr, Settings.Secure.NOTIFICATIONS_BEHAVIOUR, 0);
        mNotificationsBeh = (ListPreference) mPrefSet.findPreference("notifications_behaviour");
        mNotificationsBeh.setValue(String.valueOf(CurrentBeh));
        mNotificationsBeh.setSummary(mNotificationsBeh.getEntry());
        mNotificationsBeh.setOnPreferenceChangeListener(this);

        CurrentBeh = Settings.Secure.getInt(mCr, Settings.Secure.STATUS_BAR_BEHAVIOUR, 0);
        mStatusBarBeh = (ListPreference) mPrefSet.findPreference("status_bar_expand_beh");
        mStatusBarBeh.setValue(String.valueOf(CurrentBeh));
        mStatusBarBeh.setSummary(mStatusBarBeh.getEntry());
        mStatusBarBeh.setOnPreferenceChangeListener(this);
        
        /* Notication Shade Dim Behavior */
//        mNotificationShadeDim = (CheckBoxPreference) mPrefSet.findPreference(NOTIFICATION_SHADE_DIM);
//        mNotificationShadeDim.setChecked((Settings.System.getInt(getActivity().getApplicationContext().getContentResolver(),
//                Settings.System.NOTIFICATION_SHADE_DIM, ActivityManager.isHighEndGfx() ? 1 : 0) == 1));


    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == mBattStyle) {
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_BATT_STYLE,
                    Integer.valueOf((String) newValue));
            return true;
        } else if (preference == mStatusBarNotifCount) {
            Settings.System.putInt(mCr, Settings.System.STATUSBAR_NOTIF_COUNT,
                    ((CheckBoxPreference) preference).isChecked() ? 0 : 1);
            return true;
//        } else if (preference == mNotificationShadeDim) {
//            Settings.System.putInt(mCr, Settings.System.NOTIFICATION_SHADE_DIM,
//                    ((CheckBoxPreference) preference).isChecked() ? 0 : 1);
//            return true;
        } else if (preference == mNotificationsBeh) {
            String val = (String) newValue;
            Settings.Secure.putInt(mCr, Settings.Secure.NOTIFICATIONS_BEHAVIOUR,
                    Integer.valueOf(val));
            mNotificationsBeh.setSummary(mNotificationsBeh.getEntries()[mNotificationsBeh.findIndexOfValue(val)]);
            return true;
        } else if (preference == mStatusBarBeh) {
            String val = (String) newValue;
            Settings.Secure.putInt(mCr, Settings.Secure.STATUS_BAR_BEHAVIOUR,
                    Integer.valueOf(val));
            mStatusBarBeh.setSummary(mStatusBarBeh.getEntries()[mStatusBarBeh.findIndexOfValue(val)]);
            return true;
        } else if (preference == mOperatorIcon) {
            Settings.Secure.putInt(mCr, Settings.Secure.USE_OPERATOR_ICON,
                    ((CheckBoxPreference) preference).isChecked() ? 0 : 1); 
            return true;
        }
        return false;
    }
}
